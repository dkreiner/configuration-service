package entitiy

import (
	"gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/config"
)

type Configuration struct {
	config.BaseConfig `mapstructure:",squash"`
	Topic             string `mapstructure:"topic"`

	ConfigMap struct {
		Name      string `mapstructure:"name"`
		NameSpace string `mapstructure:"namespace"`
	} `mapstructure:"configmap"`
}
