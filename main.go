package main

import (
	"gitlab.com/gaia-x/data-infrastructure-federation-services/por/configuration-service/entitiy"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/config"
	"os"
)

var conf entitiy.Configuration

func getDefaults() map[string]any {
	return map[string]any{
		"isDev": false,
	}
}

func main() {
	// Init Logger
	InitializeLogger()

	// Load Config
	err := config.LoadConfig("CONFIGURATIONSERVICE", &conf, getDefaults())
	if err != nil {
		Logger.Error(err)
		os.Exit(0)
	}

	listen()
}
