package main

import "sync"

func listen() {
	var wg sync.WaitGroup

	wg.Add(1)

	// Start Rest API server
	go startServer(&conf.Port, &wg)

	// Start Messaging
	go startMessaging(&wg)

	wg.Wait()
}
