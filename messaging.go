package main

import (
	"github.com/cloudevents/sdk-go/v2/event"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/messaging/cloudeventprovider"
	"os"
	"sync"
)

func startMessaging(group *sync.WaitGroup) {
	defer group.Done()
	Logger.Info("start messaging!")

	client, err := cloudeventprovider.NewClient(cloudeventprovider.Sub, conf.Topic)
	if err != nil {
		Logger.Error(err)
		os.Exit(1)
	}

	defer func() {
		if err := client.Close(); err != nil {
			Logger.Error(err)
		}
	}()

	err = client.Sub(handleOffering)
	if err != nil {
		Logger.Error(err)
		os.Exit(1)
	}
}

func handleOffering(event event.Event) {
	//TODO currently unused
}
